<?php
/****** incluir archivo que contiene la función de conexión a la Base de Datos ******/
include "../../funciones.php";

/****** Recibir ID y guardarlo en variable******/
$id    = $_GET['id'];

/****** Crear conexión con la Base de Datos ******/
$link   = conexion();

/****** Preparar Sentencia SQL ******/
$sql    = "DELETE FROM carreras WHERE id = $id";

/****** Ejecutar sentencia SQL y luego evaluar resultado ******/
mysqli_query($link, $sql) or die(mysqli_error($link));

/* Las sentencias del tipo DELETE no devuelven un listado, por lo tanto
 * no es necesario guardar los resultados en una variable ($res)
 * Y debido a esto hacemos la evaluación utilizando la cantidad de filas afectadas
 * Si $filas_affectadas > 0 entonces se ejecutó la consulta (se borró el registro)
 * */

$filas_affectadas = mysqli_affected_rows($link); //Cantidad de filas afectadas en la última consulta ejecutada
if ($filas_affectadas > 0) {
    echo "<script>",
            "alert('Registro Eliminado');",
            "window.location.replace('../listado/index.php');",
         "</script>";
} else {
    echo "<script>",
            "alert('Ha ocurrido un error, por favor intente nuevamente');",
            "window.location.replace('../listado/index.php');",
         "</script>";
}

/****** Cerrar conexión a la Base de Datos  ******/
mysqli_close($link);
?>